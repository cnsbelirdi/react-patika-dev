import React from "react";
import '@testing-library/jest-dom'
import {render, screen} from '@testing-library/react'
import Header from "./Header";
import EmojiResults from "./EmojiResults";
import filterEmoji from "./filterEmoji";

test("Header component should be rendered without crashing.", () => {
  render(<Header />);
  const header = screen.getByText(/Emoji Search/i);
  expect(header).toBeInTheDocument();
});

test("Emoji List should be rendered without crashing when page is loaded.", () => {
  const emojies = filterEmoji("", 20)
  render(<EmojiResults  emojiData={emojies}/>);
  const emojiTitle = screen.getAllByTitle("emojies");
  expect(emojiTitle.length).toEqual(20);
});

test("Emoji List should be rendered while filtering.", () => {
  const filteredEmoji = filterEmoji("heart", 20)
  render(<EmojiResults  emojiData={filteredEmoji}/>);
  const emojiTitle = screen.getAllByTitle("emojies");
  expect(emojiTitle.length).toEqual(filteredEmoji.length);
});

test("Emoji should be copied when any of them was clicked.", () => {
  render(<EmojiResults  emojiData={filterEmoji("", 20)}/>);
  const emojiTitle = screen.getAllByTitle("emojies");
  expect(emojiTitle[0]).toHaveAttribute("data-clipboard-text");
});
